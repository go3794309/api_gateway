package handler

import (
	"todo_services/api/models"
	pb "todo_services/protos"
	"context"
	"github.com/gin-gonic/gin"
)


func (h *Handler) CreateTodo(c *gin.Context) {
	request := models.CreateTodoRequest{}

	if err := c.ShouldBind(&request); err != nil {
		panic(err)
	}

	resp, err := h.todoService.Create(context.Background(), &pb.CreateTodoRequest{
		Name:     request.Name,
		Deadline: request.Deadline,
		Status:   request.Status,
	})
	if err != nil {
		panic(err)
	}

	c.JSON(200, resp)
}
