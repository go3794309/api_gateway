package handler

import (
	"todo_services/config"
	pb "todo_services/protos"
)

type Handler struct {
	todoService pb.TodoServiceClient
}


func New(cfg config.Config, todoService pb.TodoServiceClient) Handler {
	return Handler{
		todoService: todoService,
	}
}
