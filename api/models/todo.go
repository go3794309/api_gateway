package models

type Todo struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Deadline string `json:"deadline"`
	Status   bool   `json:"status"`
}


type CreateTodoRequest struct {
	Name     string `json:"name"`
	Deadline string `json:"deadline"`
	Status   bool   `json:"status"`
}
