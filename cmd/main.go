package main

import (
	"todo_services/api/handler"
	"todo_services/config"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	pb "todo_services/protos"
)

func main() {
	cfg := config.Load()

	conn, err := grpc.Dial(cfg.TodoGRPCServiceHost+cfg.TodoGRPCServicePort, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		panic(err)
	}

	todoService := pb.NewTodoServiceClient(conn)

	h := handler.New(cfg, todoService)

	r := gin.New()

	
	r.POST("/todo", h.CreateTodo)

	if err = r.Run(cfg.HTTPPort); err != nil {
		panic(err)
	}

	defer conn.Close()
}
